$(function()
{
	Main.init();
});

var Main =
{
	//--------------------------------------------------------------------------------------------------------

	init: function()
	{
		this.siteLoad();
	},

	//--------------------------------------------------------------------------------------------------------

	setLang: function(langId)
	{
		$.cookies.set("langId", langId, { path: "/", domain: "." + _domain, hoursToLive: 0 });
		window.location.reload(true);
	},

	//--------------------------------------------------------------------------------------------------------

	logout: function()
	{
		var settings =
		{
			fileName: "/CALogout/",
			showOkMessage: false,
			okCallBackFn: function(){window.location.reload(true);}
		}
		AjaxRequest.send(settings);
	},

	//--------------------------------------------------------------------------------------------------------

	menuClick: function(anchor)
	{
		window.location.hash = anchor;
		Main.siteLoad();
	},

	//--------------------------------------------------------------------------------------------------------

	siteLoad: function()
	{
		//Проверяем, есть ли хеш в адресе
		if(window.location.hash !== "")
		{
			//Достаем текущий URL страницы
			var currentUrl = document.location.href;
			//Достаем хэш без решетки
			var urlHash = currentUrl.substring(currentUrl.indexOf("#") + 1);
			//Разбиваем urlHash по знаку "-"
			var urlHashArray = urlHash.split("-");

			switch(urlHashArray[0])
			{
				//О компании
				case "about":
				{
					//Проматываем к О компании
					Main.scrollTo("about");
					break;
				}

				default:
				{
					break;
				}
			}
		}
	},

	//--------------------------------------------------------------------------------------------------------

	scrollTo: function(anchor)
	{
		var heightAdminPanel = $(".adminPanelCarrier_div").height();
		var offTop = heightAdminPanel;

		$.scrollTo("#" + anchor, 1250,
		{
			offset: { top: -offTop, left: 0 }
		});
	},

	//--------------------------------------------------------------------------------------------------------

	contactsFormSend: function(fThis)
	{
		var settings =
		{
			fileName: "/CAContactsFormSend/",
			parameters: { q: fThis },
			messageType: "alert",
			showOkMessage: true,
			showErrorMessage: true,
			okCallBackFn: "Main.contactsFormSend_ok",
		}
		AjaxRequest.send(settings);
	},

	contactsFormSend_ok: function(data)
	{
		window.location.reload(true);
	},

	//--------------------------------------------------------------------------------------------------------

	weWillContactYouFormSend: function(fThis)
	{
		var settings =
		{
			fileName: "/CAWeWillContactYouFormSend/",
			parameters: { q: fThis },
			messageType: "alert",
			showOkMessage: true,
			showErrorMessage: true,
			okCallBackFn: "Main.weWillContactYouFormSend_ok",
		}
		AjaxRequest.send(settings);
	},

	weWillContactYouFormSend_ok: function(data)
	{
		window.location.reload(true);
	},

	//--------------------------------------------------------------------------------------------------------

	subscribeForActionFormSend: function(fThis)
	{
		var settings =
		{
			fileName: "/CASubscribeForActionFormSend/",
			parameters: { q: fThis },
			messageType: "alert",
			showOkMessage: true,
			showErrorMessage: true,
			okCallBackFn: "Main.subscribeForActionFormSend_ok",
		}
		AjaxRequest.send(settings);
	},

	subscribeForActionFormSend_ok: function(data)
	{
		window.location.reload(true);
	},

	//--------------------------------------------------------------------------------------------------------

	showAndHideBlock: function(fThis, selector)
	{
		if ($(selector).is(":hidden"))
		{
			$(fThis)
			.removeClass("rolled")
			.addClass("deployed");
			$(selector).slideDown("slow");
		}
		else
		{
			$(fThis)
			.removeClass("deployed")
			.addClass("rolled");
			$(selector).slideUp("slow");
		}
	},

	//--------------------------------------------------------------------------------------------------------

	showAndHideBlock2: function(fThis, selector)
	{
		if ($(selector).hasClass("show"))
		{
			$(fThis)
			.removeClass("deployed")
			.addClass("rolled");

			$(selector).removeClass("show").addClass("hide");
		}
		else
		{	
			$(fThis)
			.removeClass("rolled")
			.addClass("deployed");

			$(selector).removeClass("hide").addClass("show");
		}
	}

	//--------------------------------------------------------------------------------------------------------

}
