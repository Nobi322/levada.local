$('.advantagesIcon .advantages').css({
	position: 'absolute',
	transform: 'translate(3em,0)'
});

$(document).ready(function () {
	$('.advantagesIcon').viewportChecker({
		classToAdd:'anim-done',
		classToRemove: 'anim-done',
		repeat: true
	});
	$('.weWillContactYouForm').viewportChecker({
		classToAdd:'anim-done',
		classToRemove: 'anim-done',
		repeat: true,
		callbackFunction: function(e, a){
			var pill = $(e).find('tr'),
				i = 0;

			if(a=='add'){
				var intervalID = setInterval(function () {

					pill.eq(i).addClass('fadeIn animated visible');
					i++;
					if(i>=pill.length){
						clearInterval(intervalID);
					}
				}, 300);
			}else{
				var intervalID2 = setInterval(function () {

					pill.eq(i).removeClass('fadeIn animated visible');
					i++;
					if(i>=pill.length){
						clearInterval(intervalID2);
					}
				}, 300);
			}

		}
	});

	/*skrollr.init({
		constants: {
			//fill the box for a "duration" of 150% of the viewport (pause for 150%)
			//adjust for shorter/longer pause
			weWillContactYouForm: '150%',
			advantagesBlock: '300p'
		}
	});*/


	var setSkrollr = function($el, data) {
	    for (var i = 0, l = data.length; i < l; i++) { // loop all data entries (scroll positions + css property & value)
	        var d = data[i], // the current data entry
	            px = d[0]; // the scroll position (in pixels)
	            css = d[1]; // the css property + value to set
	        $el.attr('data-' + px, css);
	    }
	}

	setSkrollr($('#weWillContactYouForm'), [
			[1400, 'bottom: -800px;'],
			//[1500, 'bottom: -200px;transition: all 0.01s ease;'], 
			[1800, 'bottom: 100px;transition: all 0.01s ease;'],
		]);
	setSkrollr($('.footerCD'), [
			[1400, 'bottom: -800px;'],
			//[1500, 'bottom: -200px;transition: all 0.01s ease;'], 
			[2500, 'bottom: 150px;transition: all 0.01s ease;'],
		]);
	setSkrollr($('.overlay'), [
			[1900, 'display:none;'],
			[2000, 'display:block;opacity:0.1;'],
			[2500, 'opacity:0.7;'],
		]);
	setSkrollr($('.advantagesBlock'), [[1699, 'position:relative;'], [1700, 'position:fixed;z-index:10;top:0;width:100%;']]);

	skrollr.init({
        //smoothScrolling: false,
        //forceHeight: false
    });

	//$('footer.footerCD').mousewheelStopPropagation();
});

function draw() {
  var canvas = document.getElementById('canvas');
  if (canvas.getContext){
    var ctx = canvas.getContext('2d');

    ctx.beginPath();
    ctx.moveTo(75,50);
    ctx.lineTo(100,75);
    ctx.lineTo(100,25);
    ctx.fill();
  }
}


// init animation core
//new WOW().init();

