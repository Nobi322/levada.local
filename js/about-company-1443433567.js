$(function()
{
	AboutCompany.init();
});

var AboutCompany =
{
	//--------------------------------------------------------------------------------------------------------
	
	init: function()
	{
		Scroll.initHorizontalScrollType2("#ourAwardsScroll");
		this.initTourOfFactorySliderSlider();
	},

	//--------------------------------------------------------------------------------------------------------

	initTourOfFactorySliderSlider: function()
	{
		var width = $(".tourOfFactorySlider").attr("data-max-width");
		var height = $(".tourOfFactorySlider").attr("data-max-height");
		var autoPlay = true;
		var navigationActive = true;
		
		if (1 == $(".tourOfFactorySlider div.item").length)
		{
			navigationActive = false;
			autoPlay = false;
		}

		$(".tourOfFactorySlider").slidesjs(
		{
	        width: width,
	        height: height,
	        pagination:
			{
	        	active: false
	        },
			navigation:
			{
	      		active: navigationActive,
	      		effect: "slide"
			},
			effect:
			{
				slide:
				{
					speed: 800,
				}
			},
			play:
			{
				active: false,
				swap: false,
				effect: "slide",
				interval: 8000,
				auto: autoPlay,
				pauseOnHover: true
			}
		});
	}

	//--------------------------------------------------------------------------------------------------------
}
