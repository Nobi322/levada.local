$(function ()
{
});

var Scroll =
{
	init: function ()
	{
	},

	//������������� ��������������� ������� 1-�� ����
	initHorizontalScrollType1: function(selector)
	{
		var centralBlockWidth = $(selector).width()
		//������ ���� ������� � �������
		var contentWidth = 0;
		//������������ ������ �����
		var maxDataHeight = 0;

		//�������� ��� ����� � ��������
		$("div.item", selector + " .bbContainer").each(function ()
		{
			//������� ������ ����� � ���������
   			contentWidth = contentWidth + $(this).outerWidth();
			//������ ������ �����
			var dataHeight = $(this).outerHeight();

			//���� ������������ ������ �����
   			if(dataHeight > maxDataHeight)
   			{
   				maxDataHeight = dataHeight;
   			}
		});

		//���������� ������ ����� � �������� (����� �������� �������� �� ����� ������)
		$(".bbContainer", selector).css("width", contentWidth + "px");
		//���������� ������ ����� � ��������
		$(".scroll-pane", selector + " .bannerBlock").css("height", maxDataHeight + 30 + "px");
		//���������� ������ ����� ��� �������
		$(".scroll-pane", selector + " .bannerBlock").css("height", maxDataHeight + "px");

		//������� ��� ������
		var scrollElement = $(".scroll-pane", selector)
		.bind('jsp-arrow-change', function(event, isAtTop, isAtBottom, isAtLeft, isAtRight)
		{
			//������� ����� �������
			if(true === isAtLeft)
			{
				//�������� ����� ���������
				$(".button.left", selector + " .scrollBlock1").hide();
			}
			else
			{
				//���������� ����� ���������
				$(".button.left", selector + " .scrollBlock1").show();
			}

			//������� ������ �������
			if(true === isAtRight)
			{
				//�������� ������ ���������
				$(".button.right", selector + " .scrollBlock1").hide();
			}
			else
			{
				//���������� ������ ���������
				$(".button.right", selector + " .scrollBlock1").show();
			}
		})
		.jScrollPane(
		{
			animateDuration: 300
		});

		//�������� ������ � API �������
		var scrollElementApi = scrollElement.data("jsp");

		if(contentWidth > centralBlockWidth)
		{
			//������ ���� �� ����� ��������� � �������� ��
			$(".button.left", selector + " .scrollBlock1")
			.click(function()
			{
				scrollElementApi.scrollByX(centralBlockWidth * -1, true);
			})
			.hide();

			//������ ���� �� ������ ���������
			$(".button.right", selector + " .scrollBlock1").click(function()
			{
				scrollElementApi.scrollByX(centralBlockWidth, true);
			});
		}
		else
		{
			//�������� ������������� �������
			$(".button.left", selector + " .scrollBlock1").hide();
			$(".button.right", selector + " .scrollBlock1").hide();

		}
	},

	//----------------------------------------------------------------------------------------

	//������������� ��������������� ������� 1-�� ����
	initHorizontalScrollType2: function(selector)
	{
		var centralBlockWidth = $(selector).width()
		//������ ���� ������� � �������
		var contentWidth = 0;
		//������������ ������ �����
		var maxDataHeight = 0;

		//�������� ��� ����� � ��������
		$("div.item", selector + " .bbContainer").each(function ()
		{
			//������� ������ ����� � ���������
   			contentWidth = contentWidth + $(this).outerWidth();
			//������ ������ �����
			var dataHeight = $(this).outerHeight();

			//���� ������������ ������ �����
   			if(dataHeight > maxDataHeight)
   			{
   				maxDataHeight = dataHeight;
   			}
		});

		//���������� ������ ����� � �������� (����� �������� �������� �� ����� ������)
		$(".bbContainer", selector).css("width", contentWidth + "px");
		//���������� ������ ����� � ��������
		$(".scroll-pane", selector + " .bannerBlock").css("height", maxDataHeight + 30 + "px");
		//���������� ������ ����� ��� �������
		$(".scroll-pane", selector + " .bannerBlock").css("height", maxDataHeight + "px");

		//������� ��� ������
		var scrollElement = $(".scroll-pane", selector)
		.bind('jsp-arrow-change', function(event, isAtTop, isAtBottom, isAtLeft, isAtRight)
		{
			//������� ����� �������
			if(true === isAtLeft)
			{
				//�������� ����� ���������
				$(".button.left", selector + " .scrollBlock2").hide();
			}
			else
			{
				//���������� ����� ���������
				$(".button.left", selector + " .scrollBlock2").show();
			}

			//������� ������ �������
			if(true === isAtRight)
			{
				//�������� ������ ���������
				$(".button.right", selector + " .scrollBlock2").hide();
			}
			else
			{
				//���������� ������ ���������
				$(".button.right", selector + " .scrollBlock2").show();
			}
		})
		.jScrollPane(
		{
			animateDuration: 300
		});

		//�������� ������ � API �������
		var scrollElementApi = scrollElement.data("jsp");

		if(contentWidth > centralBlockWidth)
		{
			//������ ���� �� ����� ��������� � �������� ��
			$(".button.left", selector + " .scrollBlock2")
			.click(function()
			{
				scrollElementApi.scrollByX(centralBlockWidth * -1, true);
			})
			.hide();

			//������ ���� �� ������ ���������
			$(".button.right", selector + " .scrollBlock2").click(function()
			{
				scrollElementApi.scrollByX(centralBlockWidth, true);
			});
		}
		else
		{
			//�������� ������������� �������
			$(".button.left", selector + " .scrollBlock2").hide();
			$(".button.right", selector + " .scrollBlock2").hide();

		}
	},

	//----------------------------------------------------------------------------------------

	//������������� ��������������� ������� 2-�� ����
	initScrollList: function(selector)
	{
		var centralBlockWidth = $(selector).width()
		//������ ���� ������� � �������
		var contentWidth = 0;
		//������������ ������ ����� � �������
		var maxDataHeight = 0;

		//�������� ��� ����� � ��������
		$("div.item", selector + " .bbContainer").each(function ()
		{
			//������� ������ ����� � ��������� (������ ���� ����� ������ 190px)
   			contentWidth = contentWidth + $(this).outerWidth();
			//������ ������ ����� � �������
			var dataHeight = $(this).outerHeight();

			//���� ������������ ������ ����� � �������
   			if(dataHeight > maxDataHeight)
   			{
   				maxDataHeight = dataHeight;
   			}
		});

		//���������� ������ ����� � �������� (����� �������� �������� �� ����� ������)
		$(".bbContainer", selector).css("width", contentWidth + 2 + "px");
		//���������� ������ ����� ��� �������
		$(".scroll-pane", selector + " .bannerBlock").css("height", maxDataHeight + "px");

		//������� ��� ������
		var scrollElement = $(".scroll-pane", selector)
		.bind('jsp-arrow-change', function(event, isAtTop, isAtBottom, isAtLeft, isAtRight)
		{
			//������� ����� �������
			if(true === isAtLeft)
			{
				//�������� ����� ���������
				$("img", selector + " .scrollBlock2 > .leftArrowButton").hide();
			}
			else
			{
				//���������� ����� ���������
				$("img", selector + " .scrollBlock2 > .leftArrowButton").show();
			}

			//������� ������ �������
			if(true === isAtRight)
			{
				//�������� ������ ���������
				$("img", selector + " .scrollBlock2 > .rightArrowButton").hide();
			}
			else
			{
				//���������� ������ ���������
				$("img", selector + " .scrollBlock2 > .rightArrowButton").show();
			}
		})
		.jScrollPane(
		{
			animateDuration: 300
		});

		//�������� ������ � API ������� (������ ���������� ���������� ��� ���������������������)
		_scrollElementApi = scrollElement.data("jsp");

		if(contentWidth > centralBlockWidth)
		{
			//������ ���� �� ����� ��������� � �������� ��
			$("img", selector + " .scrollBlock2 > .leftArrowButton")
			.click(function()
			{
				_scrollElementApi.scrollByX(centralBlockWidth * -1, true);
			})
			.hide();

			//������ ���� �� ������ ���������
			$("img", selector + " .scrollBlock2 > .rightArrowButton").click(function()
			{
				_scrollElementApi.scrollByX(centralBlockWidth, true);
			});
		}
		else
		{
			//�������� ������������� �������
			$("img", selector + " .scrollBlock2 > .leftArrowButton").hide();
			$("img", selector + " .scrollBlock2 > .rightArrowButton").hide();

		}
	},

	//----------------------------------------------------------------------------------------

	//������������� ������������� ������� 1-�� ����
	initVerticalScrollType1: function(selector)
	{
		//������� ��� ������
		var scrollElement = $(".vertical-scroll-pane", selector)
		.jScrollPane(
		{
			animateDuration: 300,
			mouseWheelSpeed: 50
		});
	}
};