$(function()
{
	EventView.init();
});

var EventView =
{
	//--------------------------------------------------------------------------------------------------------
	
	init: function()
	{
		this.initArticleImageSlider();
		Scroll.initHorizontalScrollType1("#otherEventScroll");
	},

	//--------------------------------------------------------------------------------------------------------

	initArticleImageSlider: function()
	{
		var width = $(".articleImageSlider").attr("data-max-width");
		var height = $(".articleImageSlider").attr("data-max-height");
		var autoPlay = true;
		var navigationActive = true;
		var paginationActive = true;
		
		if (0 == $(".articleImageSlider div.item").length)
		{
			return;
		}
		
		if (1 == $(".articleImageSlider div.item").length)
		{
			paginationActive = false;
			navigationActive = false;
			autoPlay = false;
		}

		$(".articleImageSlider").slidesjs(
		{
	        width: width,
	        height: height,
	        pagination:
			{
	        	active: paginationActive,
	      		effect: "fade"
	        },
			navigation:
			{
	      		active: navigationActive,
	      		effect: "slide"
			},
			effect:
			{
				slide:
				{
					speed: 800,
				},
				fade:
				{
					speed: 800,
				}
			},
			play:
			{
				active: false,
				swap: false,
				effect: "slide",
				interval: 8000,
				auto: autoPlay,
				pauseOnHover: true
			},
			callback:
			{
				loaded: function(number)
				{
					//������ ����� � ������ ���� �� ����������� ������
					EventView.setImgAndClickOnBackgroundPagination();
				}
			}			
		});
	},

	//������������� ����������� �� ��� ���������
	setImgAndClickOnBackgroundPagination: function ()
	{
		$(".item", ".articleImageSlider").each(function()
		{
			var slideNumber = $(this).attr("slidesjs-index");
			var imgSrc = $(this).css("background-image");

			$("a[data-slidesjs-item='" + slideNumber + "']", ".articleImageSlider ul.slidesjs-pagination li.slidesjs-pagination-item")
			.css("background-image", imgSrc);
		});
	},

	//--------------------------------------------------------------------------------------------------------

	initializedSlider: function()
	{
		$(".item", ".articleImageSlider").each(function (e)
		{
			var src = $(this).attr("data-src");
			
			EventView.slBgArray[e] = src;
		});
	}

	//--------------------------------------------------------------------------------------------------------
}
