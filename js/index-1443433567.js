$(function()
{
	Index.init();
});

var Index =
{
	//--------------------------------------------------------------------------------------------------------
	
	init: function()
	{
/*
		this.initWidthAndHeightVideo();

		$(window).resize(function()
		{
			Index.initWidthAndHeightVideo();
		});
*/
		this.initIndexSlider();
	},

	//--------------------------------------------------------------------------------------------------------

	initWidthAndHeightVideo: function()
	{
		var height = $(".headerMainPage").outerHeight();

		$(".headerMainPage video").height(height);
	},

	//--------------------------------------------------------------------------------------------------------

	initIndexSlider: function()
	{
		var width = $(".indexSlider").attr("data-max-width");
		var height = $(".indexSlider").attr("data-max-height");
		var autoPlay = true;
		var navigationActive = true;
		
		if (1 == $(".indexSlider div.item").length)
		{
			navigationActive = false;
			autoPlay = false;
		}

		$(".indexSlider").slidesjs(
		{
	        width: width,
	        height: height,
	        pagination:
			{
	        	active: false
	        },
			navigation:
			{
	      		active: navigationActive,
	      		effect: "slide"
			},
			effect:
			{
				slide:
				{
					speed: 800,
				}
			},
			play:
			{
				active: false,
				swap: false,
				effect: "slide",
				interval: 8000,
				auto: autoPlay,
				pauseOnHover: true
			}
		});
	}

	//--------------------------------------------------------------------------------------------------------
}
